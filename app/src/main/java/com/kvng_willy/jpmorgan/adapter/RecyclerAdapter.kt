package com.kvng_willy.jpmorgan.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.kvng_willy.jpmorgan.R
import com.kvng_willy.jpmorgan.data.db.entity.Albums

class RecyclerAdapter(private val statusResponse: List<Albums>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mRecyclerViewItems: List<Albums>? = statusResponse
    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var nameText: TextView = view.findViewById(R.id.nameText)

    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        try {
            val menuItemHolder = holder as MyViewHolder
            val item: Albums = mRecyclerViewItems!![position]
            menuItemHolder.nameText.text = "${item.title}"
        } catch (e: Exception) {
        }
    }
    override fun getItemCount(): Int {
        return mRecyclerViewItems!!.size
    }
}