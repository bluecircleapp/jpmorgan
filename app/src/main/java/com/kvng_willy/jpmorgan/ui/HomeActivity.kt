package com.kvng_willy.jpmorgan.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.kvng_willy.jpmorgan.R
import com.kvng_willy.jpmorgan.adapter.RecyclerAdapter
import com.kvng_willy.jpmorgan.databinding.ActivityHomeBinding
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class HomeActivity : AppCompatActivity(),KodeinAware {
    override val kodein by kodein()
    private val factory: ViewModelFactory by instance()
    private lateinit var viewmodel: ViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityHomeBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_home
        )
        viewmodel = ViewModelProvider(this, factory).get(ViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this

        viewmodel.albums.observe(this, Observer {
            val mainAdapter = RecyclerAdapter(it.sortedBy { it.title })
            binding.recyclerView.apply {
                layoutManager = LinearLayoutManager(context)
                setHasFixedSize(true)
                adapter = mainAdapter
            }
        })
    }

}