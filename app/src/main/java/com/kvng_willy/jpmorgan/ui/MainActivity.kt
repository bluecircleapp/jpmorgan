package com.kvng_willy.jpmorgan.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.kvng_willy.jpmorgan.R
import com.kvng_willy.jpmorgan.data.network.responses.AlbumResponse
import com.kvng_willy.jpmorgan.databinding.ActivityMainBinding
import com.kvng_willy.jpmorgan.utility.ApiException
import com.kvng_willy.jpmorgan.utility.NoInternetException
import com.kvng_willy.jpmorgan.utility.toast
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class MainActivity : AppCompatActivity(),KodeinAware {
    override val kodein by kodein()

    private val factory: ViewModelFactory by instance<ViewModelFactory>()
    private lateinit var viewmodel: ViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewmodel = ViewModelProvider(this,factory).get(ViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        viewmodel.albums.observe(this, Observer { it1 ->
            if(it1.isEmpty()){
                viewmodel.fetchAlbums()
            }else{
                Intent(this,HomeActivity::class.java).also {it->
                    startActivity(it)
                    finish()
                }
            }
        })

    }
}