package com.kvng_willy.jpmorgan.data.network.responses

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.sql.Date

data class AlbumResponse(
    @PrimaryKey(autoGenerate = true)
    val userId: Int?,
    var id: Int?,
    var title: String?,
)
