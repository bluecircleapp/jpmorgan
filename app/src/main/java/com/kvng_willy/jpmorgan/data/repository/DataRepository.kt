package com.kvng_willy.jpmorgan.data.repository

import android.util.Log
import com.kvng_willy.jpmorgan.data.db.AppDatabase
import com.kvng_willy.jpmorgan.data.db.entity.Albums
import com.kvng_willy.jpmorgan.data.network.MyApi
import com.kvng_willy.jpmorgan.data.network.responses.AlbumResponse
import com.kvng_willy.jpmorgan.utility.Coroutines
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DataRepository(
    private val api: MyApi,
    private val db: AppDatabase
)  {

    fun fetchAlbums(): ArrayList<AlbumResponse> {
        var albums = ArrayList<AlbumResponse>()
        api.getAlbumList().enqueue(object : Callback<ArrayList<AlbumResponse>> {

            override fun onResponse(
                call: Call<ArrayList<AlbumResponse>>,
                response: Response<ArrayList<AlbumResponse>>
            ) {
                if (response.code() == 200) {
                    albums = response.body()!!
                    for (e in albums) {
                        val album = Albums(null, e.userId, e.id, e.title)
                        Coroutines.io {
                            saveAlbum(album)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<ArrayList<AlbumResponse>>, t: Throwable) {
                //handle network failure
            }
        })
        return albums
    }
    suspend fun saveAlbum(albums: Albums) = db.getAlbumDao().upsert(albums)

    fun getAlbums() = db.getAlbumDao().getAlbums()
}